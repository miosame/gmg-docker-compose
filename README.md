# gmg-docker-compose

1. Execute: `wget https://p.miosa.me/miosame/gmg-docker-compose/raw/branch/master/docker-compose.yml` in a terminal
2. Now open the downloaded `docker-compose.yml` in a text editor
    1. Change `xx.xx.xx.xx` to the local IP of your bbq grill in your network (check your router or download the app on android and scan your network: https://play.google.com/store/apps/details?id=com.overlook.android.fing&hl=en)
    2. Change `https://your_slack_webhook_address` to the slack URL you created (resource: https://api.slack.com/messaging/webhooks and https://api.slack.com/messaging/webhooks)
    3. If you do not want slack, then remove the line 8 marked here: https://p.miosa.me/miosame/gmg-docker-compose/src/branch/master/docker-compose.yml#L8
3. Save your changes
4. Run `docker-compose up` read what comes up in the terminal and if everything went fine
5. After it sort of calmed down press: `CTRL + C`
6. Execute: `docker-compose up -d` that will make it run in the background
7. Open the IP of your VM in a browser and you should see the grill UI